# E-FIRE CTF

## Traces écrites

- [Intro Crypto](./cours/intro_crypto/README.md)
- [ARP](./cours/arp/README.md)

## Write-ups

- [Poorly Managed pt1](./wu/poorly_managed_pt1.md)
- [Encoding is not encryption](./wu/encoding.md)
- [Hidden Tweet](./wu/hidden_tweet.md)
- [Online calculator](./wu/online_calculator.md)
- [Jump Around](./wu/jump_around.md)
- [LAN Capture](./wu//lan_capture.md)
- [Exfiltration 1](./wu/exfiltration_1.md)
