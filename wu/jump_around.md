# Jump around

- [Jump around](#jump-around)
  - [Présentation chall](#présentation-chall)
  - [Write-up](#write-up)

## Présentation chall

➜ **Name** : Jump around

➜ **Catégorie** : 🧨 cracking

➜ **Contexte** :

Un simple programme qui... ne fait rien ? Il doit cacher quelque chose ! 

[Avec un fichier à DL](./files/jump_around)

## Write-up

Déjà, c'est quoi comme fichier ?

```bash
❯ file jump_around
jump_around: ELF 64-bit LSB pie executable, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, BuildID[sha1]=d5d938782a72a659420f00277253c37f96936728, for GNU/Linux 3.2.0, not stripped
```

Ok c'est un exécutable. On va lui donner les permissions pour qu'on puisse l'exécuter :

```bash
# on donne les permissions
❯ chmod +x jump_around

# exécution
❯ ./jump_around
Ce programme ne fait pas grand chose ...
```

Super, il ne fait effectivement pas grand chose. On va le désassembler avec IDA pour regarder le code machine qu'il contient.

En ouvrant IDA, on repère 4 fonctions :

![Funcs](./files/jmp_funcs.png)

Bon tout de suite on peut dire que :

- il y a un `main`, point d'entrée classique
- une fonction `printflag` ça semble être notre destination
- et le programme fait de l'AES quelque part

Si on regarde le `main` :

![main](./files/jmp_main.png)

Et `printflag` :

![main](./files/jmp_printflag.png)

On peut dire que :

- la fonction `main` est très petite, et ne comporte qu'un "flow" d'exécution : rien ne mène à la fonction `printflag`
  - il est donc impossible de rentrer dans la fonction `printflag` si on laisse le programme s'exécuter normalement
- la fonction `printflag` fait appel aux fonctions liées à AES
  - on peut supposer qu'il ne faut pas casser le chiffrement AES dans notre cas, ce serait un peu violent comme épreuve !
  - la fonction `printflag` se sert de AES pour déchiffrer un password (sûrement le flag) qui est stocké de façon chiffré
  - on pourrait tout reverse et recoder un truc qui fait la même chose, mais y'a plus smart et plus simple

Essayons de :

- lancer le programme en débug avec `gdb`
- poser un breakpoint dès le début du programme
- run le programme et être arrêté sur le breakpoint
- obtenir l'adresse de la fonction `printflag` chargée en mémoire
- sauter arbitrairement à cette adresse (en définissant la valeur de `rip` à la main)
- reprendre l'exécution du programme

Ainsi, on force l'exécution de la fonction `printflag`.

Avec GDB :

```bash
# on lance GDB
$ gdb jump_around 

# on run le programme une première fois pour tout charger en mémoire
(gdb) run 
Starting program: /home/it4/Downloads/jump_around 
[Thread debugging using libthread_db enabled]
Using host libthread_db library "/usr/lib/libthread_db.so.1".
Ce programme ne fait pas grand chose ...
[Inferior 1 (process 2687) exited normally]
# le programme se termine

# on disass main pour obtenir l'adresse du main
(gdb) disass main 
Dump of assembler code for function main:
   0x00005555555554f8 <+0>:    push   %rbp
   0x00005555555554f9 <+1>:    mov    %rsp,%rbp
   0x00005555555554fc <+4>:    lea    0xb55(%rip),%rdi
   0x0000555555555503 <+11>:    call   0x555555555050 <puts@plt>
   0x0000555555555508 <+16>:    mov    $0x0,%eax
   0x000055555555550d <+21>:    pop    %rbp
   0x000055555555550e <+22>:    ret
End of assembler dump.
# on note que la première adresse du main est 0x00005555555554f8

# on pose un breakpoint au début du main
(gdb) b *0x00005555555554f8 
Breakpoint 1 at 0x5555555554f8

# on lance le programme
(gdb) run
Starting program: /home/it4/Downloads/jump_around 
[Thread debugging using libthread_db enabled]
Using host libthread_db library "/usr/lib/libthread_db.so.1".
Breakpoint 1, 0x00005555555554f8 in main ()
# il s'arrête sur notre breakpoint

# on disass printflag pour obtenir l'adresse de la fonction printflag
(gdb) disass printflag
Dump of assembler code for function printflag:
   0x000055555555548e <+0>:    push   %rbp
   0x000055555555548f <+1>:    mov    %rsp,%rbp
   0x0000555555555492 <+4>:    sub    $0x30,%rsp
   0x0000555555555496 <+8>:    lea    0xb91(%rip),%rax
   0x000055555555549d <+15>:    mov    %rax,-0x8(%rbp)
   0x00005555555554a1 <+19>:    movabs $0xb22d5213d5186e61,%rax
   0x00005555555554ab <+29>:    movabs $0x616dca45e2e9b493,%rdx
   0x00005555555554b5 <+39>:    mov    %rax,-0x30(%rbp)
   0x00005555555554b9 <+43>:    mov    %rdx,-0x28(%rbp)
   0x00005555555554bd <+47>:    movq   $0x0,-0x20(%rbp)
   0x00005555555554c5 <+55>:    movq   $0x0,-0x18(%rbp)
   0x00005555555554cd <+63>:    movb   $0x0,-0x10(%rbp)
   0x00005555555554d1 <+67>:    lea    0xb67(%rip),%rdi
   0x00005555555554d8 <+74>:    mov    $0x0,%eax
   0x00005555555554dd <+79>:    call   0x555555555030 <printf@plt>
   0x00005555555554e2 <+84>:    mov    -0x8(%rbp),%rdx
   0x00005555555554e6 <+88>:    lea    -0x30(%rbp),%rax
   0x00005555555554ea <+92>:    mov    %rdx,%rsi
   0x00005555555554ed <+95>:    mov    %rax,%rdi
   0x00005555555554f0 <+98>:    call   0x555555555369 <decryptAES>
   0x00005555555554f5 <+103>:    nop
   0x00005555555554f6 <+104>:    leave
   0x00005555555554f7 <+105>:    ret
End of assembler dump.
# on note que la première adresse du main est 0x000055555555548e

# on définit rip à la valeur 0x000055555555548e
(gdb) set $rip=0x000055555555548e 

# on relance l'exécution du programme
(gdb) continue
Continuing.
Bravo, voici le flag : EFREI{d3bug_jmp}.`UUUU
[Inferior 1 (process 2690) exited with code 027]
```

💡 **Flag !**
