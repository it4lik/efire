# LAN capture

- [LAN capture](#lan-capture)
  - [Présentation chall](#présentation-chall)
  - [Write-up](#write-up)

## Présentation chall

➜ **Name** : LAN capture

➜ **Catégorie** : 🌐 network

➜ **Contexte** :

On a récupéré cette capture réseau au sein d'un réseau local surveillé.  
Il semblerait que des connexions de natures différentes ont lieu au sein de ce réseau.

## Write-up

On obtient un fichier PCAP à ouvrir dans Wireshark.

🐈 **Réflexe :** on recherche d'abord :

- du trafic qui sort de l'ordinaire (protocole inconnu ou peu utilisé)
- des user/mots de passe qui circulent en clair
- des protocoles notoirement faibles en terme de sécu
- on reste ouverts à d'autres détails

💡 Après exploration de la capture, on repère l'utilisation du protocole SMTP (envoi/réception de mails) avec une configuration complètement insecure.

On repère un paquet qui fait circuler un password en base64, on l'extrait et on le décode :

![SMTP frame](./files/frame.png)

```bash
$ echo 'RUZSRUl7dW5zM2N1cmVfcHJvdG9jMGxzXzRyZV9iNGR9Cg==' | base64 -d
EFREI{uns3cure_protoc0ls_4re_b4d}
```

💡 **Flag !**
