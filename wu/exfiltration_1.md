# Exfiltration

- [Exfiltration](#exfiltration)
  - [Présentation chall](#présentation-chall)
  - [Write-up](#write-up)

## Présentation chall

➜ **Name** : Exfiltration

➜ **Catégorie** : 🌐 network

➜ **Contexte** :

On a intercepté le trafic d'une machine qui semle infecté par un virus. Nous avons peur que des données soient exfiltrées de façon discrète depuis cette machine.

[Avec une capture PCAP à télécharger.](./files/exfiltration_icmp.pcapng)

## Write-up

🐈 **Réflexe 1 :** on recherche d'abord :

- du trafic qui sort de l'ordinaire (protocole inconnu ou peu utilisé)
- des user/mots de passe qui circulent en clair
- des protocoles notoirement faibles en terme de sécu
- on reste ouverts à d'autres détails

🐈 **Réflexe 2 :** apparemment on recherche une exfiltration de données, on va donc être aussi attentif à :

- des caractères/données qui circulent là où normalement y'en a pas
- des encodages hexadécimaux ou b64 (des trucs reconnaissables à l'oeil) là où y'en pas
- des techniques d'exfiltration classiques : HTTP, DNS, ICMP, etc

💡 **Après exploration de la capture, on repère l'utilisation du protocole ICMP (en l'occurrence, des ping) qui contient des champs de données remplis avec un seul caractère (au lieu du padding habituel)**

Un exemple de ping issu du PCAP de ce chall :

![ICMP exfiltration](./files/icmp_exf.png)

> Ce `E` est le premier `E` de `EFREI{}` : la première lettre du flag.

Un exemple de ping "normal" :


![Normal ping](./files/normal_ping.png)

Si on extrait le caractère contenu dans chacun de ces pings, dans l'ordre, caractère par caractère (un dans chaque ping) on peut reconstituer le flag :

```
EFREI{ICMP_3xf1ltr4ti0n}
```

💡 **Flag !**
