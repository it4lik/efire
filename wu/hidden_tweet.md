# Hidden tweet message

- [Hidden tweet message](#hidden-tweet-message)
  - [Présentation chall](#présentation-chall)
  - [Write-up](#write-up)

## Présentation chall

➜ **Name** : Hidden tweet message

➜ **Catégorie** : 🎨 stegano

➜ **Contexte** :

Un tweet suspect a été repéré sur X. Cacherait-il un message secret ?
 
`Hｅeeeeｅｅy wｈat the ｆreａｋ ⅰs ａll ａｂｏｕｔ Рaｌwοｒld ? #palworld` 

## Write-up

Il existe des techniques connues pour cacher une string dans une autre string composée d'homoglyphes. Un homoglyphe c'est un caractère qui ressemble à un autre.

`ｅ` est une homoglyphe de `e`, ou encore `ａ` est un homoglyphe de `a`.

Avec quelques tricks et une bonne connaissance de l'encodage UTF-8, on peut cacher une string dans une autre.

Des tas d'outils en ligne automatisent ça, par exemple : https://holloway.nz/steg/

En l'utilisant, on découvre le flag : `h1dd3n_tw33t` à mettre dans les balises de flag du CTF, il devient : `EFREI{h1dd3n_tw33t}`.

💡 **Flag !**
