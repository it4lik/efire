# Encoding is not encryption

- [Encoding is not encryption](#encoding-is-not-encryption)
  - [Présentation chall](#présentation-chall)
  - [Write-up](#write-up)

## Présentation chall

➜ **Name** : Encoding is not encryption

➜ **Catégorie** : 🔑 crypto

➜ **Contexte** :
Le flag est caché dans la chaîne de caractère suivante :  

```base64
RUZSRUl7YjY0X2lzX24wdF8zbmNyeXB0aTBufQo=
```

## Write-up

🐈 **Réflexe :** On reconnaît à l'oeil de la base64 : termine par `=`, composé de caractères alphanumériques.

Décodage de la base64 en ligne de commande :

```bash
$ echo "RUZSRUl7YjY0X2lzX24wdF8zbmNyeXB0aTBufQo=" | base64 -d
EFREI{b64_is_n0t_3ncrypti0n}
```

💡 **Flag !**
